'''
Created on Mar 17, 2017

@author: giridar
'''

import sys
from multiprocessing import Pool
from time import time
from spim_indexer import spim_indexer
from tokens import lemmatize
from ranked_retriever import ranked_retriever
from weights import maxtf, okapi
from tabulate import tabulate
from util import save, load


tablefmt = 'jira'


def read_queries(query_file):
    '''
    Read all the queries from the given file

    @param query_file: The file which contains the query
    '''
    queries = []
    with open(query_file) as qf:
        lines = []
        line = qf.readline()

        while line:  # ignore query number line
            line = qf.readline().rstrip()
            while line:  # empty line signifies the end of a query
                lines.append(line)
                line = qf.readline().rstrip()
            queries.append(' '.join(lines))

            lines = []
            line = qf.readline()

    return queries


def print_results(retriever, queries):
    '''
    Print the query and document vectors for all queries and also the top five results 

    @param retriever: The retriever object to use
    @param queries: The list of raw queries
    '''
    vectors = [None, None]
    docs = [None] * 5
    q = 1
    for query in queries:
        del(vectors[2:])
        vectors[0], vectors[1], results = retriever.retrieve(query)
        print 'Q{:d}: '.format(q) + query

        i = 1
        for result in results[:5]:
            vectors.append(result.vector)
            docs[i - 1] = [i, result.score, result.name, result.title, 'Yes']
            i += 1

        print tabulate(zip(*vectors), headers=['Terms', 'Query', 'Doc 1', 'Doc 2', 'Doc 3', 'Doc 4', 'Doc 5'], floatfmt='.3f', tablefmt=tablefmt)
        print tabulate(docs, headers=['Rank', 'Score', 'Doc Name', 'Title', 'Relevant'], floatfmt='.3f', tablefmt=tablefmt)
        print
        q += 1


if __name__ == '__main__':
    '''
    Main driver function to run assignment 3
    '''
    if len(sys.argv) < 3:
        print "ERROR: Please enter the directory with the documents"
        print "CMD FORMAT: python main.py <dir_name> <query_file> [<no_threads>]"

    else:
        doc_dir = sys.argv[1]
        query_file = sys.argv[2]
        k = 4
        if len(sys.argv) > 3:
            k = int(sys.argv[3])
        pool = Pool(k)

        indexer = spim_indexer(pool)
        start = time()
        index = indexer.build(doc_dir, lemmatize)
#        index = load('index')
        t_time = time() - start
        save(index, 'index')
        print 'Time to build uncompressed index \t: ' + '{0:.3f}'.format(t_time) + ' secs'

        queries = read_queries(query_file)
        start = time()
        print 'Weight 1'
        print '-' * 80
        retriever = ranked_retriever(index, maxtf, pool)
        print_results(retriever, queries)
        print
        print 'Weight 2'
        print '-' * 80
        retriever = ranked_retriever(index, okapi, pool)
        print_results(retriever, queries)
        t_time = time() - start
        print 'Time to query & print results \t: ' + '{0:.3f}'.format(t_time) + ' secs'
    pass
