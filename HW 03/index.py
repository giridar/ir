'''
Created on Mar 16, 2017

@author: giridar
'''

from abc import ABCMeta, abstractmethod
from collections import namedtuple


posting = namedtuple('posting', ['doc_id', 'tf'])
dict_entry = namedtuple('dict_entry', ['term', 'df', 'tf', 'postings'])
doc_entry = namedtuple('doc_entry', ['name', 'title', 'doc_len', 'max_tf'])


class index(object):
    '''
    Interface to store and search the index
    '''
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_term(self, term):
        '''
        Search for the given term in the index and return the corresponding postings list

        @param term: The term to search for in the index
        '''
        pass

    @abstractmethod
    def get_doc(self, doc_id):
        '''
        Search for the given document id and return the corresponding document info

        @param doc_id: The id of the document to search for
        '''
        pass
