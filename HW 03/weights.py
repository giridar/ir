'''
Created on Apr 7, 2017

@author: giridar
'''

from collections import namedtuple
from math import log


stats = namedtuple('stats', ['dfs', 'tfs', 'max_tf', 'doc_len'])


def maxtf(stats, index):
    '''
    MAX_TF term weighting function

    @param stats: Document statistics like TF, DF, MAX_TF and DOC_LEN
    @param index: Index level statistics like COLLECTION_SIZE, AVG_DOC_LEN
    '''
    n = len(stats.dfs)
    vector = [0] * n
    for i in xrange(n):
        tf = stats.tfs[i]
        df = stats.dfs[i]
        if tf:
            vector[i] = ((0.4 + 0.6 * log(tf + 0.5) / log(stats.max_tf + 1.0))
                         * log(index.collection_size / df) / log(index.collection_size))
    return vector


def okapi(stats, index):
    '''
    Okapi term weighting function

    @param stats: Document statistics like TF, DF, MAX_TF and DOC_LEN
    @param index: Index level statistics like COLLECTION_SIZE, AVG_DOC_LEN
    '''
    n = len(stats.dfs)
    vector = [0] * n
    for i in xrange(n):
        tf = stats.tfs[i]
        df = stats.dfs[i]
        if tf:
            vector[i] = ((0.4 + 0.6 * (tf / (tf + 0.5 + 1.5 * (stats.doc_len / index.avg_doc_len))))
                         * log(index.collection_size / df) / log(index.collection_size))
    return vector
