'''
Created on Apr 7, 2017

@author: giridar
'''

from abc import ABCMeta, abstractmethod
from collections import namedtuple


result = namedtuple('result', ['vector', 'score', 'name', 'title'])


class retriever(object):
    '''
    ABC to retrieve the relevant documents for a given query
    '''
    __metaclass__ = ABCMeta

    @abstractmethod
    def retrieve(self, query):
        '''
        Retrieve the relevant documents for a given query

        @param query: The raw query to retrieve relevant documents for
        '''
        pass
