'''
Created on Mar 20, 2017

@author: giridar
'''

from indexer import indexer
from os import listdir
from collections import defaultdict, Counter
from ucmp_index import ucmp_index
from tokens import parse, count
from index import doc_entry, dict_entry, posting


class spim_indexer(indexer):
    '''
    Class to index a given set of documents
    '''

    def __init__(self, pool):
        '''
        Constructor

        @param pool: Thread pool to use 
        '''
        self.pool = pool

    def build(self, doc_dir, tokenize):
        '''
        Build the index for the documents in the directory using given tokens

        @param doc_dir: Directory with the documents to index
        @param tokenize: Tokenization function to use 
        '''
        doc_paths = [(doc_dir, doc, tokenize) for doc in listdir(doc_dir)]
        doc_stats = self.pool.map(_parse_doc, doc_paths)

        docs = [None] * len(doc_paths)
        postings = defaultdict(list)
        term_tf = Counter()
        i = 0
        for doc_tf, doc_len, title in doc_stats:
            docs[i] = doc_entry(doc_paths[i][1], title, doc_len, doc_tf.most_common(1)[0][1])
            i += 1 # doc_id starts from 1
            for term, tf in doc_tf.iteritems():
                term_tf[term] += tf
                postings[term].append(posting(i, tf))
            
        terms = postings.keys()
        terms.sort()
        dict = [None] * len(terms)
        i = 0
        for term in terms:
            post = postings[term]
            dict[i] = dict_entry(term, len(post), term_tf[term], post)
            i += 1

        return ucmp_index(dict, docs, tokenize)


def _parse_doc(args):
    '''
    Parse the given doc, get its title, tokenize the text and count statistics

    @param args: Document parent directory, document name and the tokenization function to use
    '''
    doc_dir, doc, tokenize = args
    text, title = parse(doc_dir + '/' + doc)
    text = tokenize(text)
    tfs = count(text)
    return tfs, sum(tfs.values()), title
