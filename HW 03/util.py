'''
Created on Mar 20, 2017

@author: giridar
'''

import cPickle


def save(index, file):
    '''
    Save the index to the given file

    @param index: The index object to save
    @param file: The file name to save as
    '''
    with open(file, 'wb') as f:
        cPickle.dump(index, f, cPickle.HIGHEST_PROTOCOL)


def load(file):
    '''
    Load an index from the given file

    @param file: The file name to load from
    '''
    with open(file, 'rb') as f:
        return cPickle.load(f)


def str_cmp(s1, s2):
    '''
    Compare two given strings

    @param s1: String one
    @param s2: String two
    '''
    l1 = len(s1)
    l2 = len(s2)
    n = min(l1, l2)
    i = 0
    while i < n:
        if s1[i] != s2[i]:
            return ord(s1[i]) - ord(s2[i])
        i += 1
    return l1 - l2


def get_size(obj):
    '''
    Get the size of the object in memory

    @param obj: The object to get the size for
    '''
    return len(cPickle.dumps(obj, cPickle.HIGHEST_PROTOCOL))
