'''
Created on Mar 16, 2017

@author: giridar
'''

import re
from nltk import pos_tag
from nltk.corpus import wordnet, stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import PorterStemmer
from collections import Counter, defaultdict


title_re = re.compile('<TITLE>(.*)</TITLE>')
sgml_re = re.compile('<[^>.]+>')
boundary_re = re.compile('\W*\s+\W*|\W$')
wnl = WordNetLemmatizer()
ps = PorterStemmer()
stop_words = set(stopwords.words('english'))
tag_map = defaultdict(
    int, {'N': wordnet.NOUN, 'V': wordnet.VERB, 'J': wordnet.ADJ, 'R': wordnet.ADV})


def parse(doc):
    '''
    Pre-process the given document to get the title and remove all SGML tags

    @param doc: The document to parse
    '''
    with open(doc) as doc_in:
        text = doc_in.read().replace('\n', ' ')
        title = title_re.findall(text)[0]
        text = sgml_re.sub(' ', text)
        text = boundary_re.split(text)[1:-1]
    return text, title


def lemmatize(text):
    '''
    Lemmatize the tokens in the given document

    @param text: The text to lemmatize
    '''
    tags = pos_tag(text)
    i = 0
    for token, tag in tags:
        tag = tag_map[tag[0]]
        if tag:
            text[i] = wnl.lemmatize(token, tag).encode()
        i += 1
    return text


def stem(text):
    '''
    Stem the tokens in the given document

    @param text: The text to stem
    '''
    n = len(text)
    for i in xrange(n):
        text[i] = ps.stem(text[i]).encode()
    return text


def count(text):
    '''
    Count statistics for the given text array

    @param text: The text to count statistics for
    '''
    tfs = Counter()
    dots = 0
    for token in text:
        if token == '.':
            dots += 1
            continue
        if token not in stop_words:
            tfs[token] += 1
    return tfs
