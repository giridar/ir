Information Retrieval - Homework 03
___________________________________

Contents
--------
* main.py 				- Main file to build indices & print retrieval results
* tokens.py 			- Parses, tokenizes, lemmatizes and stems the given documents
* index.py 				- Abstract index class
* ucmp_index.py 		- Uncompressed index class
* indexer.py 			- Abstract indexer class
* spim_indexer.py 		- Single Pass In-Memory Indexer
* retriever.py 			- Abstract retriever class
* ranked_retriever.py 	- Statistical retriever based on vector relevance using term weighting
* weights.py 			- Module with max_tf & okapi weighting functions
* util.py 				- Module with utility functions


Libraries
---------
Pyhton's tabulate package and nltk package with PosTagger, WordnetLemmatizer & Stopwords modules 

Setup:
sudo apt-get install pip
pip install --user tabulate
pip install --user nltk
python
import nltk
nltk.download()
download averaged_perceptron_tagger
download wordnet
download stopwords
quit


Command
-------
python main.py <dir_name> <query_file> [<num_threads>]

dir_name 	- folder with all the documents to be indexed
query_file	- file with all the queries to be retrieved
num_threads	- number of threads to be used (optional)

eg., python main.py /people/cs/s/sanda/cs6322/Cranfield /people/cs/s/sanda/cs6322/hw3.queries 16