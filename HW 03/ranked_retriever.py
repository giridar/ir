'''
Created on Apr 11, 2017

@author: giridar
'''

from retriever import retriever, result
from tokens import count
from weights import stats
import operator
from math import sqrt


class ranked_retriever(retriever):
    '''
    Class to retrieve relevant documents to a given query and then rank them by the 
    given weighting function
    '''

    def __init__(self, index, weight, pool):
        '''
        Constructor

        @param index: The index object to use for retrieval 
        @param weight: The term weighting function to weight the document/query vectors 
        @param pool: Thread pool to use 
        '''
        self.index = index
        self.weight = weight
        self.pool = pool

    def retrieve(self, query):
        '''
        Retrieve the relevant documents for a given query

        @param query: The raw query to retrieve relevant documents for
        '''
        tf_counts = count(self.index.tokenize(query.split()))
        q_len = len(tf_counts)
        terms = [None] * q_len
        tfs = [None] * q_len
        dfs = [None] * q_len
        postings = [None] * q_len
        i = 0
        for term, tf in tf_counts.items():
            terms[i] = term
            tfs[i] = tf
            dict_entry = self.index.get_term(term)
            if dict_entry:
                dfs[i] = dict_entry.df
                postings[i] = (1, [(p.doc_id, [p.tf])
                                   for p in dict_entry.postings])
            else:
                # weight should be 0
                dfs[i] = self.index.collection_size
                postings[i] = (1, [])
            i += 1
        postings = merge_postings(postings, self.pool)

        query_vector = normalize(self.weight(stats(dfs, tfs, tf_counts.most_common(1)[
                                 0][1], self.index.avg_doc_len), self.index))
        d_len = len(postings)
        results = [None] * d_len
        i = 0
        for p in postings:
            doc = self.index.get_doc(p[0])
            doc_vector = normalize(self.weight(
                stats([1] * q_len, p[1], doc.max_tf, doc.doc_len), self.index))
            results[i] = result(
                doc_vector, score(query_vector, doc_vector), doc.name, doc.title)
            i += 1
        results.sort(key=operator.attrgetter('score'), reverse=True)

        return terms, query_vector, results


def merge_postings(postings, pool):
    '''
    Merge the given list of postings into one

    @param postings: List of postings to merge
    @param pool: Thread pool to use
    '''
    while len(postings) > 1:
        n = len(postings)
        if n % 2:
            postings.append(None)
            n += 1
        postings = pool.map(
            or_postings, [(postings[i], postings[i + 1]) for i in xrange(0, n, 2)])

    return postings[0][1]


def or_postings(args):
    '''
    Merge two postings lists by doing an OR/union operation

    @param args: A tuple of the given two postings
    '''
    t1, t2 = args
    if not t2:
        return t1

    i = 0
    j = 0
    s1, l1 = t1
    s2, l2 = t2
    n1 = len(l1)
    n2 = len(l2)
    postings = []

    while i < n1 and j < n2:
        p1 = l1[i]
        p2 = l2[j]
        if p1[0] == p2[0]:
            postings.append((p1[0], p1[1] + p2[1]))
            i += 1
            j += 1
        elif p1[0] < p2[0]:
            postings.append((p1[0], p1[1] + [0] * s2))
            i += 1
        else:
            postings.append((p2[0], [0] * s1 + p2[1]))
            j += 1

    while i < n1:
        p1 = l1[i]
        postings.append((p1[0], p1[1] + [0] * s2))
        i += 1

    while j < n2:
        p2 = l2[j]
        postings.append((p2[0], [0] * s1 + p2[1]))
        j += 1

    return (s1 + s2, postings)


def normalize(vector):
    norm_factor = sqrt(sum([w * w for w in vector]))
    return [w / norm_factor for w in vector]


def score(query, doc):
    '''
    Compute the relevance score between a document and a query

    @param query: Query vector
    @param doc: Document vector
    '''
    return sum([query[i] * doc[i] for i in xrange(len(query))])
