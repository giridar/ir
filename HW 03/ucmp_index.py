'''
Created on Mar 16, 2017

@author: giridar
'''

from index import index
from util import str_cmp


class ucmp_index(index):
    '''
    Class to store and search the uncompressed index
    '''

    def __init__(self, dict, docs, tokenize):
        '''
        Constructor

        @param dict: List of dictionary/term entries
        @param docs: List of document entries
        @param tokenize: Tokenization function to use
        '''
        self.dict = dict
        self.docs = docs
        self.tokenize = tokenize
        self.collection_size = len(docs)
        self.avg_doc_len = sum(
            [doc.doc_len for doc in docs]) / self.collection_size

    def get_term(self, term):
        '''
        Search for the given term in the index and return the corresponding postings list

        @param term: The term to search for in the index
        '''
        l = 0
        r = len(self.dict) - 1
        term = self.tokenize([term])[0]
        while l <= r:
            m = (l + r) / 2
            c = str_cmp(term, self.dict[m].term)
            if c == 0:
                return self.dict[m]
            elif c < 0:
                r = m - 1
            else:
                l = m + 1
        return None

    def get_doc(self, doc_id):
        '''
        Search for the given document id and return the corresponding document info

        @param doc_id: The id of the document to search for
        '''
        return self.docs[doc_id - 1]
