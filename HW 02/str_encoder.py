'''
Created on Mar 17, 2017

@author: giridar
'''


class block_code(object):
    '''
    Class to encode & decode a string in block coding
    '''

    @staticmethod
    def encode(ip):
        '''
        Encode the given string using block encoding
        '''
        op = [None] * 2 * len(ip)
        i = 0
        for s in ip:
            op[i:i + 2] = [chr(len(s)), s]
            i += 2
        return ''.join(op)


class front_code(object):
    '''
    Class to encode & decode a string in front coding
    '''

    @staticmethod
    def encode(ip):
        '''
        Encode the given string using front coding
        '''
        op = [None] * 3 * len(ip)
        p = ''
        i = 0
        for s in ip:
            j = 0
            n = min(len(p), len(s))
            while j < n and s[j] == p[j]:
                j += 1
            op[i:i + 3] = [chr(j), chr(len(s) - j), s[j:]]
            p = s
            i += 3
        return ''.join(op)
