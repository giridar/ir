Information Retrieval - Homework 02
___________________________________

Contents
--------
* main.py 			- Main file to build indices, compress & print statistics
* tokenizer.py 		- Parses, tokenizes, lemmatizes and stems the given documents
* indexer.py 		- Abstract indexer class
* spim_indexer.py 	- Single Pass In-Memory Indexer
* posting.py 		- Class to hold posting document id and term frequency
* index.py 			- Abstract index class
* ucmp_index.py 	- Uncompressed index class
* cmp_index.py 		- Compressed index class
* dict_entry.py 	- Single entry in the dictonary class
* doc_entry.py 		- Single document information class
* int_encoder.py 	- Module with classes to encode integers (Unary, Gamma & Delta)
* str_encoder.py 	- Module with classes to encode strings (Block Code, Front Code)
* compressor.py 	- Index compressor class
* util.py 			- Module with utility functions


Libraries
---------
Pyhton's nltk package with PosTagger, WordnetLemmatizer, PorterStemmer & Stopwords modules

Setup:
sudo apt-get install pip
pip install nltk
python
import nltk
nltk.download()
download averaged_perceptron_tagger
download wordnet
download stopwords
quit

Note: Permission denied to install nltk in csgrads1 system


Command
-------
python main.py <dir_name> [<no_blocks>] [<no_threads>]

dir_name 	- the folder with all the documents to be indexed
no_blocks	- number of blocks to be used (optional)
no_threads	- number of threads to be used (optional)

eg., python main.py /people/cs/s/sanda/cs6322/Cranfield 8 4