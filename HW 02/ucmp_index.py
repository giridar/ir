'''
Created on Mar 16, 2017

@author: giridar
'''

from index import index
from util import str_cmp


class ucmp_index(index):
    '''
    Class to store and search the uncompressed index
    '''

    def __init__(self, dict, docs, tokenize):
        '''
        Constructor
        '''
        self.dict = dict
        self.docs = docs
        self.tokenize = tokenize

    def getPostings(self, term):
        '''
        Search for the given term in the index and return the corresponding postings list
        '''
        l = 0
        r = len(self.dict)
        term = self.tokenize([term])[0]
        while l <= r:
            m = (l + r) / 2
            c = str_cmp(term, self.dict[m].term)
            if c == 0:
                return self.dict[m]
            elif c < 0:
                r = m - 1
            else:
                l = m + 1
        return None

    def getDocInfo(self, doc_id):
        '''
        Search for the given document id and return the corresponding document info
        '''
        return self.docs[doc_id - 1]

    def __str__(self):
        return 'ucmp_index(\n\t' + str(self.dict) + ', \n\t' + str(self.docs) + ', \n\t' + self.tokenize + '\n)' 
