'''
Created on Mar 20, 2017

@author: giridar
'''

from os import listdir
from collections import defaultdict, Counter
from dict_entry import dict_entry
from doc_entry import doc_entry
from ucmp_index import ucmp_index
from posting import posting
from tokenizer import parse


class spim_indexer(object):
    '''
    Class to index a given set of documents
    '''

    def __init__(self, pool):
        '''
        Constructor

        @param pool: Thread pool to use 
        '''
        self.pool = pool

    def build(self, dir, tokenize):
        '''
        Build the index for the documents in the directory using given tokenizer

        @param dir: Directory with the documents to index
        @param tokenize: Tokenization function to use 
        '''
        doc_paths = [(dir + '/' + doc, tokenize) for doc in listdir(dir)]
        doc_stats = self.pool.map(parse, doc_paths)

        docs = [None] * len(doc_paths)
        postings = defaultdict(list)
        term_tf = Counter()
        i = 0
        for doc_tf, doc_len in doc_stats:
            docs[i] = doc_entry(doc_paths[i][0], doc_len, doc_tf.most_common(1)[0][1])
            i += 1 # doc_id starts from 1
            for term, tf in doc_tf.iteritems():
                term_tf[term] += tf
                postings[term] += [posting(i, tf)]
            
        terms = postings.keys()
        terms.sort()
        dict = [None] * len(terms)
        i = 0
        for term in terms:
            post = postings[term]
            dict[i] = dict_entry(term, len(post), term_tf[term], post)
            i += 1

        return ucmp_index(dict, docs, tokenize)
