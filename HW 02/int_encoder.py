'''
Created on Mar 17, 2017

@author: giridar
'''

from _collections import defaultdict


u_code = defaultdict(str)
g_code = defaultdict(str, {1:0})
d_code = defaultdict(str, {1:0})



class unary(object):
    '''
    Class to encode & decode an integer in unary coding
    '''

    @staticmethod
    def encode(ip):
        '''
        Encode the given integer in unary codes
        '''
        op = u_code[ip]
        if op != '':
            return op
    
        op = ((1 << ip) - 1) << 1
        u_code[ip] = op
        return op


class gamma(object):
    '''
    Class to encode & decode an integer in gamma coding
    '''

    @staticmethod
    def encode(ip):
        '''
        Encode the given integer in gamma codes
        '''
        op = g_code[ip]
        if op != '':
            return op
    
        l = ip.bit_length() - 1
        op = unary.encode(l)
        op = ((op + 1) << l) ^ ip
        g_code[ip] = op
        return op


class delta(object):
    '''
    Class to encode & decode an integer in delta coding
    '''

    @staticmethod
    def encode(ip):
        '''
        Encode the given integer in delta codes
        '''
        op = d_code[ip]
        if op != '':
            return op
    
        l = ip.bit_length()
        op = gamma.encode(l)
        l -= 1
        op = (op << l) ^ ((1 << l) ^ ip)
        d_code[ip] = op
        return op
