'''
Created on Mar 16, 2017

@author: giridar
'''

import re
from nltk import pos_tag
from nltk.corpus import wordnet, stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import PorterStemmer
from collections import Counter, defaultdict
from time import time


sgml = re.compile('<[^>.]+>')
space = ' '
boundary = re.compile('\W*\s+\W*|\W$')
wnl = WordNetLemmatizer()
ps = PorterStemmer()
stop_words = set(stopwords.words('english'))
tag_map = defaultdict(int, {'N':wordnet.NOUN, 'V':wordnet.VERB, 'J':wordnet.ADJ, 'R':wordnet.ADV})


def parse(args):
    '''
    Pre-process the given document to remove SGML tags
    '''
    doc_path, tokenize = args
    with open(doc_path) as doc:
        text = doc.read()
        text = sgml.sub(space, text)
        text = tokenize(boundary.split(text)[1:-1])
        tfs = Counter()
        dots = 0
        for token in text:
            if token == '.':
                dots += 1
                continue
            if token not in stop_words:
                tfs[token] += 1
        return tfs, len(text) - dots


def lemmatize(text):
    '''
    Lemmatize the tokens in the given document
    '''
    tags = pos_tag(text)
    i = 0
    for token, tag in tags:
        tag = tag_map[tag[0]]
        if tag:
            text[i] = wnl.lemmatize(token, tag).encode()
        i += 1
    return text


def stem(text):
    '''
    Stem the tokens in the given document
    '''
    n = len(text)
    for i in xrange(n):
        text[i] = ps.stem(text[i]).encode()
    return text
