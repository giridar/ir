'''
Created on Mar 17, 2017

@author: giridar
'''

from dict_entry import dict_entry
from doc_entry import doc_entry
from cmp_index import cmp_index
from math import ceil
import struct


class compressor(object):
    '''
    Class to compress a given index
    '''

    def __init__(self, pool):
        '''
        Constructor

        @param pool: Thread pool to use 
        '''
        self.pool = pool

    def compress(self, index, str_cmpr, int_cmpr, k):
        '''
        Compress the given index
        '''
        n = len(index.dict)
        m = int(ceil(1.0 * n / k))
        dict_strs = [None] * m
        dict = self.pool.map(compress_term, [(index.dict[j:j + k], str_cmpr, int_cmpr) for j in xrange(0, n, k)])
        term_ptr = 0
        for i in xrange(m):
            dict_strs[i] = dict[i].term
            dict[i].term = term_ptr
            term_ptr += len(dict_strs[i])
    
        n = len(index.docs)
        m = int(ceil(1.0 * n / k))
        doc_strs = [None] * m
        docs = self.pool.map(compress_doc, [(index.docs[j:j + k], str_cmpr, int_cmpr) for j in xrange(0, n, k)])
        name_ptr = 0
        for i in xrange(m):
            doc_strs[i] = docs[i].name
            docs[i].name = name_ptr
            name_ptr += len(doc_strs[i])

        return cmp_index(''.join(dict_strs), dict, ''.join(doc_strs), docs, k, index.tokenize, str_cmpr, int_cmpr)


def compress_term(args):
    '''
    Compress the given dictionary entry
    '''
    entries, str_cmpr, int_cmpr = args
    k = len(entries)
    terms = [None] * k
    df = [None] * k
    tf = [None] * k
    gaps = [None] * k
    i = 0
    for entry in entries:
        terms[i] = entry.term
        df[i] = int_cmpr.encode(entry.df)
        tf[i] = int_cmpr.encode(entry.tf)
        gaps[i] = compress_postings(entry.postings, int_cmpr)
        i += 1
    return dict_entry(str_cmpr.encode(terms), struct.pack('>'+'I'*k, *df), struct.pack('>'+'I'*k, *tf), gaps)


def compress_postings(postings, int_cmpr):
    '''
    Compress the gaps and TFs of the given postings list
    '''
    prev = 0
    n = len(postings) * 2
    gaps = [None] * n
    i = 0
    for posting in postings:
        gaps[i] = int_cmpr.encode(posting.doc_id - prev)
        gaps[i+1] = int_cmpr.encode(posting.tf)
        prev = posting.doc_id
        i += 2
    return struct.pack('>'+'I'*n, *gaps)


def compress_doc(args):
    '''
    Compress the given document info
    '''
    entries, str_cmpr, int_cmpr = args
    k = len(entries)
    names = [None] * k
    doc_len = [None] * k
    max_tf = [None] * k
    i = 0
    for entry in entries:
        names[i] = entry.name
        doc_len[i] = int_cmpr.encode(entry.doc_len)
        max_tf[i] = int_cmpr.encode(entry.max_tf)
        i += 1
    return doc_entry(str_cmpr.encode(names), struct.pack('>'+'I'*k, *doc_len), struct.pack('>'+'I'*k, *max_tf))
