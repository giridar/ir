'''
Created on Mar 16, 2017

@author: giridar
'''


class posting(object):
    '''
    Class to hold a single entry in the postings list
    '''

    def __init__(self, doc_id, tf):
        '''
        Constructor
        '''
        self.doc_id = doc_id
        self.tf = tf
