'''
Created on Mar 16, 2017

@author: giridar
'''

from abc import ABCMeta, abstractmethod


class index(object):
    '''
    Interface to store and search the index
    '''
    __metaclass__ = ABCMeta
    
    @abstractmethod
    def getPostings(self, term):
        '''
        Search for the given term in the index and return the corresponding postings list
        '''
        pass

    @abstractmethod
    def getDocInfo(self, doc_id):
        '''
        Search for the given document id and return the corresponding document info
        '''
        pass
