'''
Created on Mar 16, 2017

@author: giridar
'''


class dict_entry(object):
    '''
    Class to hold a single entry in the dictionary
    '''

    def __init__(self, term, df, tf, postings):
        '''
        Constructor
        '''
        self.term = term
        self.df = df
        self.tf = tf
        self.postings = postings

    def __str__(self):
        return 'dict_entry(' + self.term + ', ' + str(self.df) + ', ' + str(self.tf) + ', ' + str(self.postings) + ')' 
