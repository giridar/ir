'''
Created on Mar 20, 2017

@author: giridar
'''

import cPickle


def save(index, file):
    with open(file, 'wb') as f:
        cPickle.dump(index, f, cPickle.HIGHEST_PROTOCOL)


def load(file):
    with open(file, 'rb') as f:
        return cPickle.load(f)


def str_cmp(s1, s2):
    l1 = len(s1)
    l2 = len(s2)
    n = min(l1, l2)
    i = 0
    while i < n:
        if s1[i] != s2[i]:
            return ord(s1[i]) - ord(s2[i])
        i += 1
    return l1 - l2


def get_size(obj):
    return len(cPickle.dumps(obj, cPickle.HIGHEST_PROTOCOL))
