'''
Created on Mar 17, 2017

@author: giridar
'''

from multiprocessing import Pool
from time import time
from spim_indexer import spim_indexer
from tokenizer import lemmatize, stem
from compressor import compressor, compress_postings
from int_encoder import gamma, delta
from str_encoder import block_code, front_code
from util import load, save, get_size
from sys import maxint
import sys


def build_index(dir, tokenize, indexer, compressor, k, str_cmpr, int_cmpr, file):
    start = time()
    ucmp_index = indexer.build(dir, tokenize)
#    ucmp_index = load(file+'.uncompress')
    t_time = time() - start
    print 'Time to build uncompressed index \t: ' + '{0:.2f}'.format(t_time) + ' secs'
    save(ucmp_index, file + '.uncompress')

    start = time()
    cmp_index = compressor.compress(ucmp_index, str_cmpr, int_cmpr, k)
#    cmp_index = load(file+'.compress')
    t_time = time() - start
    print 'Time to build compressed index \t\t: ' + '{0:.2f}'.format(t_time) + ' secs'
    save(cmp_index, file + '.compress')

    return ucmp_index, cmp_index


def print_stats(ucmp_index, cmp_index):
    print 'Size of uncompressed index \t\t\t: ' + str(get_size(ucmp_index)) + ' bytes'
    print 'Size of compressed index \t\t\t: ' + str(get_size(cmp_index)) + ' bytes'
    print 'Number of inverted lists \t\t\t: ' + str(len(ucmp_index.dict))

    print 'Term DF TF Length(Uncmp) Length(Cmp)'
    l = ['Reynolds', 'NASA', 'Prandtl', 'flow', 'pressure', 'boundary', 'shock']
    for t in l:
        p = ucmp_index.getPostings(t.lower())
        if p:
            print t, p.df, p.tf, get_size(p.postings), get_size(compress_postings(p.postings, cmp_index.int_cmpr))

    print 'Statistics for NASA'    
    t = 'NASA'.lower()
    p = ucmp_index.getPostings(t)
    print 'DF \t\t\t\t\t\t\t\t\t:', p.df
    print 'First three documents in postings list'
    print 'DOC_ID \t TF \t DOC_LEN \t MAX_TF'
    for posting in p.postings[:3]:
        d = ucmp_index.getDocInfo(posting.doc_id)
        print posting.doc_id, '\t\t', posting.tf, '\t\t', d.doc_len, '\t\t', d.max_tf

    max_df = 0
    max_terms = []
    min_df = maxint
    min_terms = []
    with open('terms', 'wb') as f:
        for dict_entry in ucmp_index.dict:
            f.write(dict_entry.term + ' - ' + str(dict_entry.postings[0].doc_id) + '\n')
            if dict_entry.df == max_df:
                max_terms += [dict_entry.term]
            elif dict_entry.df > max_df:
                max_df = dict_entry.df
                max_terms = [dict_entry.term]
            if dict_entry.df == min_df:
                min_terms += [dict_entry.term]
            elif dict_entry.df < min_df:
                min_df = dict_entry.df
                min_terms = [dict_entry.term]
    print 'Term(s) with largest DF \t\t\t:'
    print max_terms
    print 'Term(s) with lowest DF \t\t\t\t:'
    print min_terms

    doc_len = ('', 0)
    max_tf = ('', 0)
    for doc_entry in ucmp_index.docs:
        if doc_entry.doc_len > doc_len[1]:
            doc_len = (doc_entry.name, doc_entry.doc_len)
        if doc_entry.max_tf > max_tf[1]:
            max_tf = (doc_entry.name, doc_entry.max_tf)
    print 'Document with largest doc_len \t\t:'
    print doc_len[0]
    print 'Document with largest max_tf \t\t:'
    print max_tf[0]


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "ERROR: Please enter the directory with the documents"
        print "CMD FORMAT: python main.py <dir_name> [<no_blocks>] [<no_threads>]"

    else:
        dir = sys.argv[1]
        n = 4
        if len(sys.argv) > 2:
            n = int(sys.argv[2])
        k = 8
        if len(sys.argv) > 3:
            k = int(sys.argv[3])

        pool = Pool(n)
        indexer = spim_indexer(pool)
        compressor = compressor(pool)
    
        print 'Version 1'
        print '------------------------------------------------------'
        ucmp_index, cmp_index = build_index(dir, lemmatize, indexer, compressor, k, block_code, gamma, 'index_version1')
        print_stats(ucmp_index, cmp_index)
    
        print
        print '******************************************************'
        print
    
        print 'Version 2'
        print '------------------------------------------------------'
        ucmp_index, cmp_index = build_index(dir, stem, indexer, compressor, k, front_code, delta, 'index_version2')
        print_stats(ucmp_index, cmp_index)
    pass
