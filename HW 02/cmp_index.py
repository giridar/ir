'''
Created on Mar 16, 2017

@author: giridar
'''

from index import index


class cmp_index(index):
    '''
    Class to store and search the compressed index
    '''

    def __init__(self, dict_str, dict, doc_str, docs, k, tokenize, str_cmpr, int_cmpr):
        '''
        Constructor
        '''
        self.dict_str = dict_str
        self.dict = dict
        self.doc_str = doc_str
        self.docs = docs
        self.k = k
        self.tokenize = tokenize
        self.str_cmpr = str_cmpr
        self.int_cmpr = int_cmpr

    def getPostings(self, term):
        '''
        Search for the given term in the index and return the corresponding postings list
        '''
        pass

    def getDocInfo(self, doc_id):
        '''
        Search for the given document id and return the corresponding document info
        '''
        pass

    def __str__(self):
        return 'cmp_index(\n\t' + str(self.k) + ', \n\t' + self.dict_str + ', \n\t' + str(self.dict) + ', \n\t' + self.doc_str + ', \n\t' + str(self.docs) + ', \n\t' + self.tokenize + '\n)'
