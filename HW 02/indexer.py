'''
Created on Mar 17, 2017

@author: giridar
'''

from abc import ABCMeta, abstractmethod


class indexer(object):
    '''
    ABC to index a given set of documents
    '''
    __metaclass__ = ABCMeta
    
    @abstractmethod
    def build(self, dir, tokenizer):
        '''
        Build the index for the documents in the directory using given tokenizer
        '''
        pass
