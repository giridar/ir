'''
Created on Mar 16, 2017

@author: giridar
'''


class doc_entry(object):
    '''
    Class to hold the information of a single document
    '''

    def __init__(self, name, doc_len, max_tf):
        '''
        Constructor
        '''
        self.name = name
        self.doc_len = doc_len
        self.max_tf = max_tf

    def __str__(self):
        return 'doc_entry(' + self.name + ', ' + str(self.doc_len) + ', ' + str(self.max_tf) + ')' 
