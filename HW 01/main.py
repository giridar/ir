'''
Created on Jan 30, 2017

@author: giridar
'''

import os, re, sys
from collections import Counter
from nltk.stem import PorterStemmer
from time import time

tag = re.compile('<[^>.]+>')
space = ' '
dot = re.compile('([A-Z])\.(?=[ A-Z.])')
group = '\g<1>'
boundary = re.compile('\W?\s+\W?')
porter = PorterStemmer()

def countTokens(dir):
    n = 0
    t_count = 0
    t_counter = Counter()
    for file in os.listdir(dir):
        with open(dir + '/' + file) as doc:
            text = doc.read()

            # Replace SGML tags with space
            text = tag.sub(space, text)

            # Remove dots within an acronym 
            text = dot.sub(group, text)

            # Split tokens by space & remove all non-alpha numeric characters
            # at the start and end of a word
            tokens = boundary.split(text.lower())

            t_count += len(tokens)
            t_counter.update(tokens)
        n += 1

        # Remove all empty tokens
        t_count -= t_counter['']
        del t_counter['']
    return (n, t_count, t_counter)

def countStems(t_counter):
    s_counter = Counter()
    for t, c in t_counter.iteritems():
        # Stem every key in the token counter
        s_counter.update({porter.stem(t):c})
    return s_counter

def printStats(n, count, counter, tt):
    print 'No. of words\t\t\t\t:', count
    print 'No. of unique words\t\t\t:', len(counter)
    print 'No. words with single count\t\t:', counter.values().count(1)
    print '30 most frequent words\t\t\t:'
    print counter.most_common(30)
    print 'Avg. no. of words per doc.\t\t:', count * 1.0 / n
    print 'Avg. no. of unique words per doc.\t:', len(counter) * 1.0 / n
    print 'Time taken\t\t\t\t: %.3f s' % tt

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "ERROR: Please enter the directory with the documents"
        print "CMD FORMAT: python main.py <dir_name>"

    else:
        start = time()
        (n, t_count, t_counter) = countTokens(sys.argv[1])
        t_time = time() - start
    
        print 'Tokens'
        print '======'
        printStats(n, t_count, t_counter, t_time)
    
        start = time()
        s_counter = countStems(t_counter)
        s_time = time() - start
    
        print
        print 'Stems'
        print '====='
        printStats(n, t_count, s_counter, s_time)

    pass
