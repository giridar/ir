Information Retrieval - Homework 01
___________________________________

Contents
--------
* main.py - Python source file to perform Tokenization & Stemming

Libraries
---------
Pyhton's nltk.stem package with PorterStemmer module
Note: nltk module already available in csgrads1 system

Command
-------
python main.py <dir>

dir - the folder with all the documents to be tokenized

eg., python main.py /people/cs/s/sanda/cs6322/Cranfield